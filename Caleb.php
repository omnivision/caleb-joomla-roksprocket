<?php
/**
 * @version   $Id$
 * @author	RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

class RokSprocket_Provider_Caleb extends RokSprocket_Provider_AbstarctJoomlaBasedProvider
{
	/**
	 * @static
	 * @return bool
	 */
	public static function isAvailable()
   	{
		return defined('JPATH_SITE'); 
   	}

	/**
	 * @param array $filters
	 * @param array $sort_filters
	 */
	public function __construct($filters = array(), $sort_filters = array())
	{
		parent::__construct('caleb');
		$this->setFilterChoices($filters, $sort_filters);
	}

	public function getItems() {
		$bits = array('lang'=>'', 'types'=>'', 'categories'=>'','countries'=>'');

		$mainframe = JFactory::getApplication();
		$language =  $mainframe->getCfg('language');
		$langCode =  strtoupper(substr($language,0,2));
		$source = "http://www.om.org/rss/resources.rss?plain=1";

		if (in_array(4,(array)$this->params->get('caleb_resource_types')))
			$bits['lang']="&text=LANG_".$langCode;

		if ($this->params->get('caleb_resource_types'))
			foreach ($this->params->get('caleb_resource_types') as $resource_type_id)
				$bits['types'] .= '&mediaTypeId='.$resource_type_id;

		if ($this->params->get('caleb_categories'))
			if (is_array($this->params->get('caleb_categories')))
				foreach ($this->params->get('caleb_categories') as $category_id)
					$bits['categories'] .= "&categoryId=".$category_id;

		if ($this->params->get('caleb_countries')) {
			foreach ($this->params->get('caleb_countries') as $country_id) {
				$bits['countries'] .= "&countryId=".$country_id;
			}
		}
		$source_exact = $source.$bits['lang'].$bits['types'].$bits['categories'].$bits['countries'];
		$feed = simplexml_load_file($source_exact);
		$maxitems = $feed->count(); // return all items 
		/* If no results are returned, remove the parameter that has been specified from the URL.
	 	 * hopefully that will bring some results. */
		if ($maxitems < 1) {
			switch ($this->params->get('caleb_fallback_skip')) {
				case "countries": $bits['countries'] = ''; break;
				case "categories": $bits['categories'] = ''; break;
				case "language": $bits['lang'] = ''; break;
			}
			$source_fallback = $source.$bits['lang'].$bits['types'].$bits['categories'].$bits['countries'];
			$feed = simplexml_load_file($source_fallback);
		}

		$collection = new RokSprocket_ItemCollection();
		//if( ! is_wp_error( $feed ) ) {
			$maxitems = $feed->count(); // return all items 

			foreach($feed->channel->item  as $raw_item ) {
				$item = $this->convertRawToItem( $raw_item );
				$collection[$item->getId()] = $item;
			}
		//}

		$this->mapPerItemData($collection);
		return $collection;
	}

	public function set_cache_time( $seconds ) {
		return $this->params->get( 'caleb_cache', 12 ) * 60 * 60;
	}

	protected function convertRawToItem($raw_item, $dborder = 0) {
		$om = $raw_item->children('http://app.om.org/dtd/rss.dtd');
		$item = new RokSprocket_Item();
		
		$item->setProvider('caleb');
		
		$item->setId("caleb-" . md5($om->id[0]) ); // md5 the ID to remove weird characters
		$item->setAlias((String)$om->id[0]);
		$item->setTitle((String)$om->title[0]);
		$item->setDate($om->creationDate);
		
		$item->setPublished(true);
		
		$item->setText((String)$om->description[0]);
		$item->setAuthor((String)$om->author[0]);
		$item->setCategory((String)$om->categories[0]);
		
		$item->setHits(null);
		$item->setRating(null);
		$item->setMetaKey(null);
		$item->setMetaDesc(null);
		$item->setMetaData(null);
		//set up images array
		$images = array();
		$image = new RokSprocket_Item_Image();
		$source = (String)$om->thumbnailUrl;
		$source = str_replace("/t/",'/m/',$source) ; // Get medium sized image.
		if( $this->params->get( 'caleb_download_images', 0 ) ) {
			$source = $this->download_image_copy( $source );
		}
		$image->setSource( $source );
			$image->setCaption((String)$om->thumbnailDescription);
			$image->setWidth((Int)$om->thumbnailWidth);
			$image->setHeight((Int)$om->thumbnailHeight);
			$image->setIdentifier('image_thumbnail');
		try {
			$image->setCaption( $this->get_image_caption( $images_matches[0][0] ) );
			$image->setAlttext( $this->get_image_caption( $images_matches[0][0] ) );
		} catch (Exception $e ){}
			$images[$image->getIdentifier()] = $image;
			$item->setImages($images);
			$item->setPrimaryImage($images['image_thumbnail']);

		$primary_link = new RokSprocket_Item_Link();
		$primary_link->setUrl( $raw_item->guid );
		$primary_link->getIdentifier('article_link');
		$item->setPrimaryLink( $primary_link );

		return $item;
	}

	public function get_image_caption( $image_tag ) {
		preg_match( "/alt=\"(.+?)\"/", $image_tag, $matches );
		return isset( $matches[1] ) ? $matches[1] : '';
	}

	function download_image_copy( $url ) {
		if (!isset($url)) return;

		$mainframe = JFactory::getApplication();
		$tmp_path =  $mainframe->getCfg('tmp_path');
		$_info = pathinfo( $url );		
		$filename =  'm'.$_info['basename'];
		$filepath = $tmp_path . DS . $filename ;
		
		/* here is the key to this caching business 
		 * - don't download it again if you've already got it! */
		if (file_exists($filepath)) return $filepath;
		
		$tmp = $tmp_path.DS.time().".tmp";
		try {
			if ($image_contents = @file_get_contents($url))
				file_put_contents($tmp, $image_contents);
		} catch (exception $e) {
			@unlink($tmp);
			return $url;
		}
		if (!file_exists($tmp)) return $url;

		$pathparts = pathinfo( $tmp );
		$result = copy( $tmp, $filepath );
		@unlink( $tmp );

		return ( $result ) ? $filepath : $url;
	}

	/**
	 * @param $id
	 *
	 * @return string
	 */
	protected function getArticleEditUrl($id)
	{
		return;
	}

	/**
	 * @return array the array of image type and label
	 */
	public static function getImageTypes()
	{
		return;
	}

	/**
	 * @return array the array of link types and label
	 */
	public static function getLinkTypes()
	{
		return;
	}

	/**
	 * @return array the array of link types and label
	 */
	public static function getTextTypes()
	{
		return;
	}
}
