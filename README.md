# Caleb Provider for RockSprocket in Joomla 
This code snippet allows you to pull stories, photos, etc from OM's Caleb and feed them into a nifty RokSprocket widget.

## What it does ##
1. Choose resource types, countries, categories
2. Provide title, image, text, link
3. Local custom URL - TODO 
4. Download images to resize them - TODO
5. Local cache, time configurable - TODO

## What it doesn't do ##
1. RokSprocket Filters
2. Custom ordering - latest is always first, sorry
3. Preview in the admin

## Installation ##
You'll need to place these files manually - there's no installer

Place them in: {joomla_dir}/components/com__roksprocket/lib/RokSprocket/Provider/Caleb/

make a symlink to Caleb.php in the parent folder
```ln -s Caleb.php ../Caleb.php```

## Screenshots ##
![alt text](http://webserver.omnivision.om.org/wdemo/wp-content/uploads/2013/11/Screen-shot-2013-11-28-at-13.15.291.png "Two RokSprocket items from Caleb")

## Notes ##
The admin gets a bit confused about what articles are displayed. Always check the real website to see the change.

Caleb.php:32 - If articles are among the requested types, add ?text=LANG_{lang_code}

Caleb.php:59 - If no results are returned, remove the fallback variable from the list (example: countries). Say, you've selected articles from Spain, then you view the page in Spanish - there's no results. Choose Country as your fallback option, so then you will get some international Spanish articles instead of nothing. Or, you could select Language as your fallback, then you would just get the articls about spain in English. 
